﻿"""
Collection of parsers which read various bridge-related file formats.
"""

__all__ = ["PBNParser", "DealerParser"]

from endplay.parsers.pbn import PBNParser
from endplay.parsers.dealer import DealerParser

