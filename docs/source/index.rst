.. endplay documentation master file, created by
   sphinx-quickstart on Sun Oct 31 09:38:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to endplay's documentation!
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   pages/readme/intro
   
   pages/readme/building_and_installing
   
   pages/readme/overview_of_submodules
   
   pages/readme/tutorial

   pages/reference/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
